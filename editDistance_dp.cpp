#include <iostream>
#include <vector>

using namespace std;

int min(int a, int b, int c)
{
	return (a<b && a<c ? a : b<c ? b : c);
}

int editDistance(string& str1, string& str2, int m, int n)
{
	vector< vector <int> > editDistanceMatrix(m+1, vector<int>(n+1));
	for (int i=0; i<=m; i++)
	{
		for (int j=0; j<=n; j++)
		{
			if (i==0)
			{
				editDistanceMatrix[i][j] = j;
				continue;
			}
			if (j==0)
			{
				editDistanceMatrix[i][j] = i;
				continue;
			}
			if (str1[i-1] == str2[j-1])
			{
				editDistanceMatrix[i][j] = editDistanceMatrix[i-1][j-1];
			}
			else
			{
				editDistanceMatrix[i][j] = 1 + min(editDistanceMatrix[i-1][j-1], editDistanceMatrix[i][j-1], editDistanceMatrix[i-1][j]);
			}
		}
	}
	return editDistanceMatrix[m][n];
}

int main()
{
	string str1;
	string str2;
	cin >> str1;
	cin >> str2;
	int distance;
	distance = editDistance(str1, str2, str1.length()-1, str2.length()-1);
	cout << distance << endl;
	return 0;
}
