#include <iostream>
#include <cmath>

using namespace std;

int lps(string& arr, int l, int r)
{
	if (l == r)
	{
		return 1;
	}
	if (r-l == 1)
	{
		if (arr[l] == arr[r])
		{
			return 2;
		}
/*
		else
		{
			return 0;
		}
*/
	}
	if (arr[l] == arr[r])
	{
		return (lps(arr, l+1, r-1) + 2);
	}
	return (max(lps(arr, l+1, r), lps(arr, l, r-1)));
}

int main()
{
	string input;
	cin >> input;
	int l = lps(input, 0, input.length()-1);
	cout << l << endl;
}
