#include <iostream>
#include <vector>
#include <map>
using namespace std;

void printVec(vector<int> ar)
{
	for (int i=0; i<ar.size(); i++)
	{
		cout << ar[i];
	}
	cout << endl;
}

int main()
{
	int n;
	cin >> n;
	vector<int> input(n);
	for (int i=0; i<n; i++)
	{
		cin >> input[i];
	}
	map<int, vector<int> > mukul;
	int i=0;
	int max = -1;
	if (n%2 != 0)
	{
		i=1;
		max = input[0];
	}
	while (i+1<n)
	{
		cout << "Doing for " << i << " and " << i+1 << endl;
		if (input[i] > input[i+1])
		{
			mukul[input[i]].push_back(input[i+1]);
		}
		else
		{
			mukul[input[i+1]].push_back(input[i]);
		}
		i += 2;
	}
	for(auto it = mukul.cbegin(); it != mukul.cend(); ++it)
	{
		cout << it->first << "->";
		for (int i=0; i<it->second.size(); i++)
		{
			cout << (it->second)[i] << " " ;
		}
		cout << endl;
	}
}
