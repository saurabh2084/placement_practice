#include <iostream>
#include <cmath>

using namespace std;

int numTrials(int numEggs, int numFloors)
{
	if (numFloors == 0 || numFloors == 1)
	{
		return numFloors;
	}
	if (numEggs == 1)
	{
		return numFloors;
	}
	int min = pow(2,30);
	for (int i=1; i<=numFloors; i++)
	{
		int trials = max(numTrials(numEggs-1, i-1), numTrials(numEggs, numFloors-i));
		if (trials < min)
		{
			min = trials;
		}
	}
	return min+1;
}

int main()
{
	int numEggs;
	int numFloors;
	cin >> numEggs;
	cin >> numFloors;
	int trials = numTrials(numEggs, numFloors);
	cout << trials << endl;
	return 0;
}
