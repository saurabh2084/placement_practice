#include <iostream>
#include <vector>
#include <cmath>

using namespace std;

int lcs(string &arr1, string &arr2, int n1, int n2)
{
	vector< vector<int> > lcsMat(n1+1, vector<int>(n2+1));
	for (int i=0; i<=n1; i++)
	{
		for (int j=0; j<=n2; j++)
		{
			if (i == 0 || j == 0)
			{
				lcsMat[i][j] = 0;
				continue;
			}
			if (arr1[i-1] == arr2[j-1])
			{
				cout << arr1[i-1];
				lcsMat[i][j] = 1 + lcsMat[i-1][j-1];
			}
			else
			{
				lcsMat[i][j] = max(lcsMat[i-1][j], lcsMat[i][j-1]);
			}
		}
	}
	return lcsMat[n1][n2];
}

int main()
{
	string arr1;
	string arr2;
	cin >> arr1;
	cin >> arr2;
	int l;
	l = lcs(arr1, arr2, arr1.length(), arr2.length());
	cout << l << endl;
	return 0;
}
