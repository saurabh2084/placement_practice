#include <iostream>
#include <cstring>
#include <algorithm>

using namespace std;

string add(string a, string b)   // a is shorter than b
{
	cout << "Add called for " << a <<" " << b << endl;
	string result;
	int carry = 0;
	int index = a.length();
	int alen = a.length();
	int blen = b.length();
//	while (index--)
//	while (alen-- && blen--)
	for (int index=1; index<=alen; index++)
	{
		int sum = 0;
		sum = a[alen - index] - '0' + b[blen - index] - '0' + carry;
		char toAppend = (sum%10) + '0';
		result += toAppend;
		carry = sum/10;
		/*
		cout << "Iter no : " << index << endl;
		cout << "Sum : " << sum << endl; 
		cout << "Carry : " << carry << endl; 
		cout << "Result : " << result << endl; 
		*/
	}
	index = b.length() - a.length();
	while (index--)
	{
		int sum = 0;
		sum = b[index] - '0' + carry;
		char toAppend = (sum%10) + '0';
		result += toAppend;
		carry = sum/10;
	}
	if (carry)
	{
		char toAppend = carry + '0';
		result += toAppend;
	}
	reverse(result.begin(), result.end());
	cout << "Returning " << a << " + " << b << " as: " << result << endl;
	return result;
}

string uniProduct(string a, char b)
{
	string result;
	int index = a.length();
	int carry = 0;
	while (index--)
	{
		int sum = 0;
		sum = (a[index] - '0')*(b - '0') + carry;
		char toAppend = (sum%10) + '0';
		result += toAppend;
		carry = sum/10;
	}
	if (carry)
	{
		char toAppend = carry + '0';
		result += toAppend;
	}
	reverse(result.begin(), result.end());
	cout << "Returning " << a << "*" << b << " as: " << result << endl;
	return result;
}

string multiply(string a, string b)
{
	int index = a.length()-1;
	string result = uniProduct(b, a[index]);
	while(index--)
	{
		string uniProd = uniProduct(b,a[index]);
		for (int i=1; i<a.length()-index; i++)
		{
			uniProd += '0';
		}
		//result = add(result, uniProduct(b,a[index]) + '0');
		result = add(result, uniProd);
	}
	cout << "Returning " << a << "*" << b << "as: " << result << endl;
	return result;
}

int main()
{
	string a,b;
	cout << "Enter the two numbers one by one: " << endl;
	cin >> a >> b;
	cout << "Choose operation" << endl;
	cout << "1. Add" << endl;
	cout << "2. Multiply" << endl;
	int choice;
	cin >> choice;
	string sum,prod;
	switch(choice)
	{
		case 1: if (a.length() < b.length())
				{
					sum = add(a,b);
				}
				else
				{
					sum = add(b,a);
				}
				cout << "Sum is: " << sum << endl;
				break;

		case 2: if (a.length() < b.length())
				{
					prod = multiply(a,b);
				}
				else
				{
					prod = multiply(b,a);
				}
				cout << "Product is: " << prod << endl;
				break;

		default: cout << "Wrong choice" << endl;
				 break;
	}
}
