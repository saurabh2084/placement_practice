#include <iostream>
#include <stack>

using namespace std;

void printStack(stack<int>& s)
{
	if(s.empty())
	{
		return;
	}
	int top = s.top();
	cout << top << " ";
	s.pop();
	printStack(s);
	s.push(top);
}

void pushAtBottom(stack<int>& s, int val, int size)
{
//	if (s.empty())
	if (size == 0)
	{
		s.push(val);
		return;
	}
	int top = s.top();
	s.pop();
	pushAtBottom(s, val, size-1);
	s.push(top);
}

void reverseStack(stack<int>& s, int i)
{
	if (i==0)
	{
		return;
	}
	int top = s.top();
	s.pop();
	pushAtBottom(s, top, i-1);
	cout << "top element after pushing " << top << " to bottom of stack: " << s.top() << endl; 
	printStack(s);
	cout << endl;
	reverseStack(s, i-1);
}

int main()
{
	int n;
	cin >> n;
	stack<int> s;
	for (int i=0; i<n; i++)
	{
		int val;
		cin >> val;
		s.push(val);
	}
	cout << "Stack before reverse" << endl;
	printStack(s);
	cout << endl;
	reverseStack(s, n);
	cout << "Stack after reverse" << endl;
	printStack(s);
	cout << endl;
}
