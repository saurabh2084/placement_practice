#include <iostream>
#include <vector>
#include <stdlib.h>
#include <queue>

using namespace std;
// Input format: 
// noOfVertices (n)
// noOfEdges (e)
// e lines denoting edges (Vertex numbers starting from zero)
// traversal type: 0 for BFS, 1 for DFS

enum Color {
	white,
	grey,
	black
};

typedef struct listNode
{
	int vertex;
	struct listNode* next;
}node;

typedef struct vertexNode
{
	int value;
	Color color;
	int parent;
}vNode;

void printList(node* head)
{
	while(head != NULL)
	{
		cout << head->vertex << " -> ";
		head = head->next;
	}
}

void printAdjList(vector<node*> adjHeadList, int n)
{
	for (int i=0; i<n; i++)
	{
		cout << i << " -> ";
		printList(adjHeadList[i]);
		cout << endl;
	}
}

node* listInsert(node* head, int val)
{
//	node* newNode = (node*) malloc(sizeof(node));
	node* newNode = new node;
	newNode->vertex = val;
	newNode->next = NULL;
	if(!head)
	{
		cout << "Head found as NULL. Returning newNode as head" << endl;
		head = newNode;
	}
	else
	{
		node* temp = head;
		while(temp->next != NULL)
		{
			temp = temp->next;
		}
		temp->next = newNode;
	}
	return head;
}

void DFS(vector<vNode>& vNodes, vector<node*>& adjHeadList, vNode v)
{
	cout << v.value << " ";
	node* temp = adjHeadList[v.value];
	while (temp)
	{
		if (vNodes[temp->vertex].color == white)
		{
			vNodes[temp->vertex].color = grey;
			vNodes[temp->vertex].parent = v.value;
			DFS(vNodes, adjHeadList, vNodes[temp->vertex]);
		}
		temp=temp->next;
	}
	vNodes[v.value].color = black;
}

void graphDFS(vector<vNode> vNodes, vector<node*> adjHeadList, int n)
{
	for (int i=0; i<n; i++)
	{
		if (vNodes[i].color == white)
		{
			vNodes[i].color = grey;
			DFS(vNodes, adjHeadList, vNodes[i]);
		}
	}
}

void graphBFS(vector<vNode> vNodes, vector<node*> adjHeadList, int n)
{
	cout << "BFS not implemented" << endl;
	for (int i=0; i<n; i++)
	{

	}
}

int main()
{
	int n;
	int e;
	cin >> n;
	cin >> e;
	vector<node*> adjHeadList(n);
	vector<vNode> vNodes(n);
	for (int i=0; i<n; i++)
	{
		vNodes[i].value = i;
		vNodes[i].color = white;
		vNodes[i].parent = i;
	}
	cout << "List printed" << endl;
	for (int i=0; i<e; i++)
	{
		// Add edge from a to b
		int a;
		int b;
		cin >> a >> b;
		adjHeadList[a] = listInsert(adjHeadList[a], b);
		adjHeadList[b] = listInsert(adjHeadList[b], a);
	}
	printAdjList(adjHeadList, n);
	bool choice;
	cin >> choice;
	switch(choice)
	{
		case 0: cout << "BFS Chosen" << endl;
			 	graphBFS(vNodes, adjHeadList, n);
				break;

		case 1: cout << "DFS Chosen" << endl;
				graphDFS(vNodes, adjHeadList, n);
				cout << endl;
				break;
	}
	return 0;
}
