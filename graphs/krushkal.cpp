#include <bits/stdc++.h>
#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

typedef struct adjacentNode
{
	int target;
	int weight;
	adjacentNode(int& t, int& w)
	{
		target = t;
		weight = w;
	}
}adjNode;

typedef struct Heapnode
{
	int id;
	int distance;
	Heapnode(int& i, int& d)
	{
		id = i;
		distance = d;
	}
}hpNode;

class mycomparison
{
	bool reverse;
	public:
	mycomparison(const bool& revparam=false)
	{reverse=revparam;}
	bool operator() (const hpNode& lhs, const hpNode&rhs) const
	{
		if (reverse) 
			return (lhs.distance > rhs.distance);
		else return (lhs.distance < rhs.distance);
	}
};

struct CompareWeight 
{
	bool operator()(adjNode const & p1, Person const & p2) {
		// return "true" if "p1" is ordered before "p2", for example:
		return p1.age < p2.age;
	}
};

void printMap(map<int, list<adjNode> > myMap)
{
	cout << "Printing map" << endl;
	map<int, list<adjNode> >::iterator iter;
	for (iter = myMap.begin(); iter != myMap.end(); iter++)
	{
//		cout << "Key: " << iter->first << endl << "Values:" << endl;
		cout << iter->first << ":";
		list<adjNode>::iterator list_iter;
		for (list_iter = iter->second.begin(); list_iter != iter->second.end(); list_iter++)
			cout << " (" << list_iter->target << "," << list_iter->weight << ")" << " ";
		cout << endl;
	}
	cout << endl;
}

template<typename T>
void printVec(vector<T> arr)
    {
    for (unsigned int i=0; i<arr.size(); i++)
        {
        cout << arr[i] << " ";
    }
    cout << endl;
}

template<typename T>
const char* getClassName(T) 
{
	return typeid(T).name();
}

void insertEdge(map<int, list<adjNode> >& adjMap, int source, int target, int weight)
{
	cout << "Insert edge from " << source << " to " << target << " with weight " << weight << endl;
	map<int, list<adjNode> >::iterator iter;
	iter = adjMap.find(source);
	if (iter == adjMap.end())   // Case where source is absent from map
	{
		cout << " Case where source is absent from map" << endl;
		list<adjNode> tempList;
		tempList.emplace_back(target, weight);
		adjMap[source] = tempList;
		list<adjNode> tempList1;
		tempList1.emplace_back(source, weight);
		adjMap[target] = tempList1;
	}
	else
	{
		cout << " Case where source is present in map" << endl;
		list<adjNode> temp = (iter->second);
		list<adjNode>::iterator listIter = find_if(temp.begin(), temp.end(), [&target] (const adjNode& node) { return node.target == target; });
		printMap(adjMap);
		if(listIter != temp.end())    //  Case where source and destination both are present in Map
		{
			cout << "Adding duplicate target" << endl;
			cout << "listIter->target: " << listIter->target << "listIter->weight: " << listIter->weight << endl;
			temp.emplace_back(target, weight);
			adjMap[target].emplace_back(source, weight);
		}
		else                                    // Case where source is present but destination is absent
		{
			cout << "Adding unique target" << endl;
			temp.emplace_back(target, weight);
			adjMap[target].emplace_back(source, weight);
		}
	}
}

vector<int> dijsktraSSSP(map<int, list<adjNode> >& adjMap, int nVertices, int nEdges, int source)
{
	initializeSS(adjMap);
	priority_queue<hpNode, mycomparison> Q;
	Q.emplace(source, 0);
	while(!(Q.empty()))
	{
		hpNode minNode = Q.top();
		Q.pop();

	}
}

int main() 
{
	/* Enter your code here. Read input from STDIN. Print output to STDOUT */   
	int t;
	cin >> t;
	while(t--)
	{
		int n;
		int e;
		cin >> n >> e;
		map<int, list<adjNode> > adjMap;
		for (int i=0; i<e; i++)
		{
			int x,y,r;
			cin >> x >> y >> r;
			insertEdge(adjMap, x, y, r);
		}
		printMap(adjMap);
		int s;
		cin >> s;
		vector<int> distances = dijsktraSSSP(adjMap, n, e, s);
		printVec(distances);
	}
	return 0;
}

