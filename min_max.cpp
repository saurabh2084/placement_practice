#include <iostream>
#include <vector>
#include <cmath>

using namespace std;

void printVec(vector<int> ar)
{
	for (int i=0; i<ar.size(); i++)
	{
		cout << ar[i];
	}
	cout << endl;
}

int main()
{
	int n;
	cin >> n;
	vector<int> input(n);
	for (int i=0; i<n; i++)
	{
		cin >> input[i];
	}
	int i=0;
	int max=-1;
	int min=pow(2,30);
	if (n%2 != 0)
	{
		i=1;
		min = input[0];
		max = input[0];
	}
	while (i+1<n)
	{
		if (input[i] < input[i+1])
		{
			if (input[i] < min)
			{
				min = input[i];
			}
			if (input[i+1] > max)
			{
				max = input[i+1];
			}
		}
		else
		{
			if (input[i] > max)
			{
				max = input[i];
			}
			if (input[i+1] < min)
			{
				min = input[i+1];
			}
		}
		i += 2;
	}
	cout << min << " " << max << endl;
}
