#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

long long int maxThree(long long int a, long long int b, long long int c)
{
	cout << "Returning max for " << a << " " << b << " " << c << endl;
	return (a>b && a>c ? a : b>c ? b : c);
}

long long int minThree(long long int a, long long int b, long long int c)
{
	return (a<b && a<c ? a : b<c ? b : c);
}

long long int secMax(long long int a, long long int b, long long int c)
{
	cout << "Returning second max for " << a << " " << b << " " << c << endl;
	long long int tempMax = maxThree(a,b,c);
	if (tempMax == a)
	{
		return (b>c ? b : c);
	}
	else if (tempMax == b)
	{
		return (a>c ? a : c);
	}
	else
	{
		return (a>b ? a : b);
	}
}

long long int maxScore(vector<int>& arr)
{
	int n=arr.size();
	vector<long long int> solution(n+3);
	for (int i=n+2; i>=0; i--)
	{
		cout << "Calling for " << i << endl;
		if (i>=n)
		{
			solution[i] = 0;
			continue;
		}
		if (i == n-1)
		{
			solution[i] = arr[i];
			continue;
		}
		if (i == n-2)
		{
			solution[i] = arr[i] + arr[i+1];
			continue;
		}
		if (i == n-3)
		{
			solution[i] = arr[i] + arr[i+1] + arr[i+2];
			continue;
		}

		solution[i] =  (maxThree(arr[i] + minThree(solution[i+2], solution[i+3], solution[i+4]), 
					arr[i] + arr[i+1] + minThree(solution[i+3], solution[i+4], solution[i+5]), 
					arr[i] + arr[i+1] + arr[i+2] + minThree(solution[i+4], solution[i+5], solution[i+6])));
	}
	return solution[0];
}

int main() 
{
	/* Enter your code here. Read input from STDIN. Print output to STDOUT */   
	int t;
	cin >> t;
	while(t--)
	{
		int n;
		cin >> n;
		vector<int> arr(n);
		for (int i=0; i<n; i++)
		{
			cin >> arr[i];
		}
		long long int score = maxScore(arr);
		cout << score << endl;
	}
	return 0;
}

