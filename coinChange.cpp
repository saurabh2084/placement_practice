#include <iostream>
#include <vector>

using namespace std;

void printVec(vector<int> arr)
{
	for (unsigned int i=0; i<arr.size(); i++)
	{
		cout << arr[i] << " ";
	}
	cout << endl;
}

int main()
{
	int numCoins;
	cin >> numCoins;
	vector<int> coinVal(numCoins);
	int sum;
	for (int i=0; i<numCoins; i++)
	{
		cin >> coinVal[i];
	}
	cin >> sum;
	vector<int> comb(sum+1);
	for (int i=0; i<numCoins; i++)
	{
		int val = coinVal[i];
		comb[val]++;
		for (int j=val; j<=sum; j++)
		{
			comb[j] += comb[j-val];
		}
		cout << "Vector after " << i+1 << " th iteration" << endl;
		printVec(comb);
	}
	cout << comb[sum] << endl;
}
