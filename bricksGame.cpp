#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

long long int maxThree(long long int a, long long int b, long long int c)
{
	cout << "Returning max for " << a << " " << b << " " << c << endl;
	return (a>b && a>c ? a : b>c ? b : c);
}

long long int minThree(long long int a, long long int b, long long int c)
{
	return (a<b && a<c ? a : b<c ? b : c);
}

long long int secMax(long long int a, long long int b, long long int c)
{
	cout << "Returning second max for " << a << " " << b << " " << c << endl;
	long long int tempMax = maxThree(a,b,c);
	if (tempMax == a)
	{
		return (b>c ? b : c);
	}
	else if (tempMax == b)
	{
		return (a>c ? a : c);
	}
	else
	{
		return (a>b ? a : b);
	}
}

long long int maxScore(vector<int>& arr, int i)
{
	cout << "Called for i:" << i << endl;
	if (i>=arr.size())
	{
		cout << "Returning 0" << endl;
		return 0;
	}
	if (i == arr.size()-1)
	{
		return (arr[i]);
	}
	if (i == arr.size()-2)
	{
		return (arr[i] + arr[i+1]);
	}
	if (i == arr.size()-3)
	{
		return (arr[i] + arr[i+1] + arr[i+2]);
	}
/*
	return (maxThree(arr[i] + secMax(maxScore(arr, i+2), maxScore(arr, i+3), maxScore(arr, i+4)), 
	                 arr[i] + arr[i+1] + secMax(maxScore(arr, i+3), maxScore(arr, i+4), maxScore(arr, i+5)), 
	                 arr[i] + arr[i+1] + arr[i+2] + secMax(maxScore(arr, i+4), maxScore(arr, i+5), maxScore(arr, i+6))));
*/
	return (maxThree(arr[i] + minThree(maxScore(arr, i+2), maxScore(arr, i+3), maxScore(arr, i+4)), 
				arr[i] + arr[i+1] + minThree(maxScore(arr, i+3), maxScore(arr, i+4), maxScore(arr, i+5)), 
				arr[i] + arr[i+1] + arr[i+2] + minThree(maxScore(arr, i+4), maxScore(arr, i+5), maxScore(arr, i+6))));
/*
	return (max(arr[i] + min(maxScore(arr, i+2), maxScore(arr, i+3)), 
				arr[i] + arr[i+1] + min(maxScore(arr, i+3), maxScore(arr, i+4)))); 
*/
}

int main() 
{
	/* Enter your code here. Read input from STDIN. Print output to STDOUT */   
	int t;
	cin >> t;
	while(t--)
	{
		int n;
		cin >> n;
		vector<int> arr(n);
		for (int i=0; i<n; i++)
		{
			cin >> arr[i];
		}
		long long int score = maxScore(arr, 0);
		cout << score << endl;
	}
	return 0;
}

