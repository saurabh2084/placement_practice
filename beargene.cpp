#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

void printVec(vector<int> arr)
    {
    for (int i=0; i<arr.size(); i++)
        {
        cout << arr[i] << " ";
    }
    cout << endl;
}

int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */   
    int n;
    cin >> n;
    string s;
    cin >> s;
    int len = n;
    vector<int> geneHash(4);
    int i=0;
    for (i=0; i<len; i++)
        {
        if (s[i] == 'A')
            {
            geneHash[0]++;
            if (geneHash[0] > len/4)
                {
                break;
            }
        }
        else if (s[i] == 'T')
            {
            geneHash[1]++;
            if (geneHash[1] > len/4)
                {
                break;
            }
        }
        else if (s[i] == 'C')
            {
            geneHash[2]++;
            if (geneHash[2] > len/4)
                {
                break;
            }
        }
        else if (s[i] == 'G')
            {
            geneHash[3]++;
            if (geneHash[3] > len/4)
                {
                break;
            }
        }
    }
    //printVec(geneHash);
    if (i==len)
        {
        cout << "0" << endl;
    }
    else
        {
            int j=i;
            for (i=len-1; i>j; i--)
            {
            if (s[i] == 'A')
                {
                geneHash[0]++;
                if (geneHash[0] > len/4)
                    {
                    break;
                }
            }
            else if (s[i] == 'T')
                {
                geneHash[1]++;
                if (geneHash[1] > len/4)
                    {
                    break;
                }
            }
            else if (s[i] == 'C')
                {
                geneHash[2]++;
                if (geneHash[2] > len/4)
                    {
                    break;
                }
            }
            else if (s[i] == 'G')
                {
                geneHash[3]++;
                if (geneHash[3] > len/4)
                    {
                    break;
                }
            }
        }
        //printVec(geneHash);
        cout << i-j+1 << endl;
    }
    return 0;
}

