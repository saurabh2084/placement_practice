#include <iostream>
#include <vector>

using namespace std;

int permuteCount;

typedef struct element
{
	bool colour;
	int dfsColour;  // 0=white, 1=grey, 2=black
}node;

void printVec(vector <vector <node> > arr)
{
	for (unsigned int i=0; i<arr.size(); i++)
	{
		for (unsigned int j=0; j<arr[i].size(); j++)
		{
			cout << arr[i][j].colour;
		}
		cout << endl;
	}
	for (unsigned int i=0; i<arr.size(); i++)
	{
		for (unsigned int j=0; j<arr[i].size(); j++)
		{
			cout << arr[i][j].dfsColour;
		}
		cout << endl;
	}
}

int n;

void dfsOnArray(vector< vector <node> >& arr, unsigned int i, unsigned int j, int count)
{
	cout << "Called dfsArray with index as " << i << "," << j << " " << endl;
	if (i<0 || j<0 || i>=arr.size() || j>=arr[i].size())
	{
		cout << "Returning false due to array out of bound" << endl;
		return;
	}
	/*
	if (arr[i][j].colour == 1)
	{
		cout << "Returning false due to 1" << endl;
		return false;
	}
	*/
	if (arr[i][j].dfsColour == 0)
	{
		/*
		if (i==arr.size()-1 && j==arr.size()-1)
		{
			return true;
		}
		*/

		arr[i][j].dfsColour = 1;
		count++;
		dfsOnArray(arr,i-1,j, count);
		dfsOnArray(arr,i,j-1, count);
		dfsOnArray(arr,i+1,j, count);
		dfsOnArray(arr,i,j+1, count);
		arr[i][j].dfsColour = 2;
		if (count == 2*n)
		{
			cout << "Incrementing permuteCount" << endl;
			permuteCount++;
			return;
		}
		return;
//		return (up || down || right || left);
	}
	cout << "Returning false due to not white colour" << endl;
	return;
}

int main()
{
	int r,c;
	cin >> r >> c;
	n=c;
	cout << "Received row and column range. Now input the matrix." << endl;
	vector< vector<node> > matrix(r, vector<node>(c));
	for (int i=0; i<r; i++)
	{
		for (int j=0; j<c; j++)
		{
			cin >> matrix[i][j].colour;
			matrix[i][j].dfsColour = 0;
		}
	}
	//printVec(matrix);
	dfsOnArray(matrix, 0, 0, 0);
	//cout << result << endl;
	cout << "permuteCount" << permuteCount << endl;
	return 0;
}
