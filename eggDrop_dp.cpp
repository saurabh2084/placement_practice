#include <iostream>
#include <cmath>
#include <vector>

using namespace std;

int numTrials(int numEggs, int numFloors)
{
	vector< vector<int> > numTrialsMatrix(numEggs+1, vector<int>(numFloors+1));
	for (int i=1; i<=numEggs; i++)
	{
		for (int j=0; j<=numFloors; j++)
		{
			if (j == 0 || j == 1)
			{
				numTrialsMatrix[i][j] = j;
			}
			else if (i == 1)
			{
				numTrialsMatrix[i][j] = j;
			}
			else
			{
				int min = pow(2,30);
				for (int k=1; k<=j; k++)
				{
					int trials = max(numTrialsMatrix[i-1][k-1], numTrialsMatrix[i][j-k]);
					if (trials < min)
					{
						min = trials;
					}
				}
				numTrialsMatrix[i][j] =  min+1;
			}
		}
	}
	return numTrialsMatrix[numEggs][numFloors];
}

int main()
{
	int numEggs;
	int numFloors;
	cin >> numEggs;
	cin >> numFloors;
	int trials = numTrials(numEggs, numFloors);
	cout << trials << endl;
	return 0;
}
