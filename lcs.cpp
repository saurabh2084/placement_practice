#include <iostream>
#include <vector>
#include <cmath>

using namespace std;

//int lcs(vector<int> &arr1, vector<int> &arr2, int n1, int n2)
int lcs(string &arr1, string &arr2, int n1, int n2)
{
	if (n1 < 0 || n2 < 0)
	{
		return 0;
	}
	if (arr1[n1] == arr2[n2])
	{
		return (1 + lcs(arr1, arr2, n1-1, n2-1));
	}
	else
	{
		return (max(lcs(arr1, arr2, n1-1, n2), lcs(arr1, arr2, n1, n2-1)));
	}
}

int main()
{
	/*
	int n;
	cin >> n;
	int m;
	cin >> m;
	vector<int> arr1(n);
	vector<int> arr2(m);
	for (int i=0; i<n; i++)
	{
		cin >> arr1[i];
	}
	for (int j=0; j<m; j++)
	{
		cin >> arr2[j];
	}
	*/
	string arr1;
	string arr2;
	cin >> arr1;
	cin >> arr2;
	int l;
//	l = lcs(arr1, arr2, n-1, m-1);
	l = lcs(arr1, arr2, arr1.length()-1, arr2.length()-1);
	cout << l << endl;
	return 0;
}
