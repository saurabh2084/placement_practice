#include <iostream>
#include <algorithm>    // std::lower_bound, std::upper_bound, std::sort
#include <vector>

using namespace std;

int getUpperBound(vector<int> arr, int value)
{
	int l=0;
	int r=arr.size()-1;
	int mid;
	while (l<=r)
	{
		mid = l + (r-l)/2;
		cout << "Mid calculated as " << mid << endl;
		if (arr[mid]==value)
		{
			cout << "Element found" << endl;
			return mid + 1;
		}
		else if (arr[mid]>value)
		{
			r=mid-1;
			cout << "r updated as " << r << endl;
		}
		else
		{
			l=mid+1;
			cout << "l updated as " << l << endl;
		}
	}
	return l;
}

int main()
{
	int n;
	cin >> n;
	vector<int> v(n);
	for (int i=0; i<n; i++)
	{
		cin >> v[i];
	}
	int val;
	cout << "Enter element whose upper bound is to be found" << endl;
	cin >> val;
	int index = getUpperBound(v, val);
	cout << index << endl;
	return 0;
}
