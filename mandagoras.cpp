#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

void printMat(vector < vector <int> > arr)
    {
    for (int i=0; i<arr.size(); i++)
        {
        for (int j=0; j<arr[i].size(); j++)
            {
            cout << arr[i][j] << " ";
        }
        cout << endl;
    }
}

long long int getExp(vector<int>& arr, int n, int s)
{
    vector < vector<long long int> > matrix (n+1, vector<long long int>(n+1));
    for (int i=1; i<=n; i++)
        {
        for (int j=n-1; j>0; j--)
        //for (int j=1; j<=n; j++)
            {
            matrix[i][j] = max(matrix[i-1][j+1], matrix[i-1][j] + (j*arr[i-1]));
            //cout << "Matrix[" << i << "][" << j << "] updated as " << matrix[i][j] << endl; 
        }
    }
    //printMat(matrix);
    return (matrix[n][1]);
}

long long int recursiveGetExp(vector<int>& arr, int n, int s)
{
    cout << "Call for (" << n << "," << s << ")" << endl;
    if (n==0)
        {
		cout << "Returning 0" << endl;
        return 0;
    }
    long long int res = (max(recursiveGetExp(arr, n-1, s+1), recursiveGetExp(arr, n-1, s) + s*arr[n-1]));
	cout << "Returning for (" << n << "," << s << ") : " << res << endl;
    return (res);
}

int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */   
    int t;
    cin >> t;
    while(t--)
        {
        int n;
        cin >> n;
        vector<int> arr(n);
        for (int i=0; i<n; i++)
            {
            cin >> arr[i];
        }
        //long long int res = getExp(arr, n, 1);
        long long int res = recursiveGetExp(arr, n, 1);
        cout << res << endl;
    }
    return 0;
}

