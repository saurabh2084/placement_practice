#include <iostream>
#include <vector>

using namespace std;

typedef struct element
{
	bool colour;
	int dfsColour;  // 0=white, 1=grey, 2=black
}node;

void printVec(vector <vector <node> > arr)
{
	for (unsigned int i=0; i<arr.size(); i++)
	{
		for (unsigned int j=0; j<arr[i].size(); j++)
		{
			cout << arr[i][j].colour;
		}
		cout << endl;
	}
	for (unsigned int i=0; i<arr.size(); i++)
	{
		for (unsigned int j=0; j<arr[i].size(); j++)
		{
			cout << arr[i][j].dfsColour;
		}
		cout << endl;
	}
}

bool dfsOnArray(vector< vector <node> >& arr, unsigned int i, unsigned int j)
{
	cout << "Called dfsArray with index as " << i << "," << j << " " << endl;
	if (i<0 || j<0 || i>=arr.size() || j>=arr[i].size())
	{
		cout << "Returning false due to array out of bound" << endl;
		return false;
	}
	if (arr[i][j].colour == 1)
	{
		cout << "Returning false due to 1" << endl;
		return false;
	}
	if (arr[i][j].dfsColour == 0)
	{
		if (i==arr.size()-1 && j==arr.size()-1)
		{
			return true;
		}
		arr[i][j].dfsColour = 1;
		bool up = dfsOnArray(arr,i-1,j);
		bool left = dfsOnArray(arr,i,j-1);
		bool down = dfsOnArray(arr,i+1,j);
		bool right = dfsOnArray(arr,i,j+1);
		arr[i][j].dfsColour = 2;
		return (up || down || right || left);
	}
	cout << "Returning false due to not white colour" << endl;
	return false;
}

int main()
{
	int r,c;
	cin >> r >> c;
	cout << "Received row and column range. Now input the matrix." << endl;
	vector< vector<node> > matrix(r, vector<node>(c));
	for (int i=0; i<r; i++)
	{
		for (int j=0; j<c; j++)
		{
			cin >> matrix[i][j].colour;
			matrix[i][j].dfsColour = 0;
		}
	}
	printVec(matrix);
	bool result = dfsOnArray(matrix, 0, 0);
	cout << result << endl;
}
