#include <iostream>
#include <cmath>
#include <vector>

using namespace std;

void printVec(vector< vector <int> > arr)
{
	for (unsigned int i=0; i<arr.size(); i++)
	{
		for (unsigned int j=0; j<arr[i].size(); j++)
		{
			cout << arr[i][j] << " ";
		}
		cout << endl;
	}
}

int lps(string& arr, int l, int r)
{
	vector< vector<int> > lpsMatrix(r, vector<int>(r));
	for (int i=0; i<r; i++)
	{
		lpsMatrix[i][i] = 1;
	}
	for (int i=l; i<r; i++)
	{
//		for (int j=r-1; j>=i; j--)
		for (int j=i; j<r; j++)
		{
			cout << "Iteration with i= " << i << " j= " << j << endl;
			if (i == j)
			{
				cout << "Case 1" << endl;
				lpsMatrix[i][j] = 1;
			}
			else if (j-i == 1 && arr[j] == arr[i])
			{
				cout << "Case 2" << endl;
				lpsMatrix[i][j] = 2;
			}
			else if (arr[i] == arr[j])
			{
				cout << "Case 3" << endl;
				lpsMatrix[i][j] = lpsMatrix[i+1][j-1] + 2;
			}
			else
			{
				cout << "Case 4" << endl;
				lpsMatrix[i][j] = max(lpsMatrix[i+1][j], lpsMatrix[i][j-1]);
			}

		}
	}
	printVec(lpsMatrix);
	return lpsMatrix[l][r-1];
}

int main()
{
	string input;
	cin >> input;
	int l = lps(input, 0, input.length());
	cout << l << endl;
}
