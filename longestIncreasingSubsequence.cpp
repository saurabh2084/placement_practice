#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

int main() 
{
	/* Enter your code here. Read input from STDIN. Print output to STDOUT */   
	int n;
	cin >> n;
	vector<int> arr(n);
	for (int i=0; i<n; i++)
	{
		cin >> arr[i];
	}
	vector<int> lisArray;
	for (int i=0; i<n; i++)
	{
		vector<int>::iterator iter = upper_bound(lisArray.begin(), lisArray.end(), arr[i]);
		if (iter==lisArray.end())
		{
			lisArray.push_back(arr[i]);
		}
		else
		{
			*iter = arr[i];
		}
	}
	cout << lisArray.size() << endl;
	return 0;
}

