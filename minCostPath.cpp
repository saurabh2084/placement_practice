#include <iostream>
#include <vector>

using namespace std;

void print2D(vector< vector<int> > arr)
{
	for (unsigned int i=0; i<arr.size(); i++)
	{
		for (unsigned int j=0; j<arr[i].size(); j++)
		{
			cout << arr[i][j] << " " ;
		}
		cout << endl;
	}
}

int min(int a, int b, int c)
{
	return (a<b && a<c? a : b<c ? b : c);
}

int main()
{
	int row;
	int col;
	cin >> row;
	cin >> col;
	vector< vector<int> > input(row, vector<int>(col));
	vector< vector<int> > minCost(row, vector<int>(col));
	for (int i=0; i<row; i++)
	{
		for (int j=0; j<col; j++)
		{
			cin >> input[i][j];
		}
	}
	for (int i=0; i<row; i++)
	{
		for (int j=0; j<col; j++)
		{
			if (i==0 && j==0)
			{
				minCost[i][j] = input[i][j];
			}
			else if (i==0)
			{
				minCost[i][j] = minCost[i][j-1] + input[i][j];
			}
			else if (j==0)
			{
				minCost[i][j] = minCost[i-1][j] + input[i][j];
			}
			else
			{
				minCost[i][j] = min(minCost[i-1][j], minCost[i][j-1], minCost[i-1][j-1]) + input[i][j];
			}
		}
	}
	print2D(input);
	print2D(minCost);
	cout << minCost[row-1][col-1] << endl;;
	return 0;
}
