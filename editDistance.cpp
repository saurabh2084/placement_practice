#include <iostream>
#include <vector>

using namespace std;

int min(int a, int b, int c)
{
	return (a<b && a<c ? a : b<c ? b : c);
}

int editDistance(string& str1, string& str2, int m, int n)
{
	if (m < 0)
	{
		return n+1;
	}
	if (n < 0)
	{
		return m+1;
	}
	if (str1[m] == str2[n])
	{
		return editDistance(str1, str2, m-1, n-1);
	}
	else
	{
		return (1 + min (editDistance(str1, str2, m-1, n-1) , editDistance(str1, str2, m, n-1) , editDistance(str1, str2, m-1, n)));
	}
}

int main()
{
	string str1;
	string str2;
	cin >> str1;
	cin >> str2;
	int distance;
	distance = editDistance(str1, str2, str1.length()-1, str2.length()-1);
	cout << distance << endl;
	return 0;
}
