#include <iostream>
#include <vector>

using namespace std;

void printVec(vector<int> arr)
{
	for (unsigned int i=0; i<arr.size(); i++)
	{
		cout << arr[i] << " ";
	}
	cout << endl;
}

void fuse(vector<int> &arr, vector<int> &temp, int start)
{
	int i=start;
	unsigned int j=0;
	while (j<temp.size())
	{
		arr[i] = temp[j];
		i++;
		j++;
	}
}

void merge(vector<int> &arr, int firstleft, int firstright, int secondleft, int secondright)
{
	int newsize = firstright + secondright - firstleft - secondleft + 2;
	vector<int> temp(newsize);
	int i=firstleft;
	int j=secondleft;
	int k=0;
	while (i<=firstright && j<=secondright)
	{
		if (arr[i] > arr[j])
		{
			temp[k] = arr[j];
			j++;
		}
		else if (arr[i] < arr[j])
		{
			temp[k] = arr[i];
			i++;
		}
		else
		{
			temp[k++] = arr[i];
			temp[k] = arr[j];
			i++;
			j++;
		}
		k++;
	}
	while (i<=firstright)
	{
		temp[k] = arr[i];
		i++;
		k++;
	}
	while (j<=secondright)
	{
		temp[k] = arr[j];
		j++;
		k++;
	}
	fuse(arr, temp, firstleft);
}

int main()
{
	int n;
	cin >> n;
	vector<int> vec(n);
	for (int i=0; i<n; i++)
	{
		cin >> vec[i];
	}
	int fl,fr,sl,sr;
	for (int step=1; step<n; step*=2)
	{
		int i=0;
		while (i+step<n)
		{
			fl = i;
			fr = i+step-1;
			i+=step;
			sl = i;
			if (i+step<n)
			{
				sr = i+step-1;
			}
			else
			{
				sr = n-1;
			}
			i+=step;
			merge(vec, fl, fr, sl, sr);
		}
	}
	printVec(vec);
	return 0;
}
