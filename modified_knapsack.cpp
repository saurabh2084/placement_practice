#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

template<typename T>
void printVec(vector<T> arr)
    {
    for (int i=0; i<arr.size(); i++)
        {
        cout << arr[i] << " ";
    }
    cout << endl;
}

int maxThree(int a, int b, int c)
    {
    //cout << "Returning max of " << a << " " << b << " " << c << endl;
    return (a>b && a>c ? a : b>c ? b : c);
}

int knapsack(vector<int>& arr, int i, int n, int sum, int k, int curr)
    {
    //cout << "Call for (" << i << "," << sum << ")" << endl;
    if (i==n)
        {
        //cout << "Returning zero for i: " << i << " sum: " << sum << endl;
        return 0;
    }
    if(sum + curr > k)
        {
        //cout << "Returning zero for i: " << i << " sum: " << sum << endl;
        //cout << "Returning sum: " << sum - arr[i] << " for i: " << i << endl;
        return sum;
    }
    //return maxThree(knapsack(arr, n-1, k - arr[n-1]) + arr[n], knapsack(arr, n-1, k), knapsack(arr, n, k-arr[n-1]) + arr[n]);
    sum += curr;
    return maxThree(knapsack(arr, i, n, sum, k, arr[i]),              // Picking same item
                    knapsack(arr, i+1, n, sum, k, arr[i]),             // Picking next item
                    knapsack(arr, i+1, n, sum, k, 0)                   // Not picking next item
                   );
}

int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */   
    int t;
    cin >> t;
    while (t--)
        {
        int n;
        int k;
        cin >> n;
        cin >> k;
        vector<int> arr(n);
        for (int i=0; i<n; i++)
            {
            cin >> arr[i];
        }
        //printVec(arr);
        int sum = knapsack(arr, 0, n, 0, k, 0);
        cout << sum << endl;
    }
    return 0;
}

