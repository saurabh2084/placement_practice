#include <iostream>
#include <stack>
#include <cmath>
#include <string>

using namespace std;

void printStack(stack<int>& s)
{
	if(s.empty())
	{
		return;
	}
	int top = s.top();
	cout << top << " ";
	s.pop();
	printStack(s);
	s.push(top);
}

int stackToInt(stack<int> s)
{
	string num;
	string temp;
	while(!s.empty())
	{
		int top = s.top();
		temp = to_string(top);
		num.append(temp);
		s.pop();
	}
	int ret = stoi(num);
	cout << "Returning from stackToInt: " << ret << endl;
	return (ret);
}

int getOnesCount(stack<int>& s, int i)
{
	cout << "Call came for i = " << i << " and stack " << endl;
	printStack(s);
	cout << endl;
	if (i==1)
	{
		if (s.top() != 0)
		{
			return 1;
		}
		return 0;
	}
	int top = s.top();
	int count = 0;
	if (top == 0)
	{
	}
	else if (top > 1)
	{
		count += ((top)*(i-1)*(pow(10,i-2))) + pow(10,i-1);
		cout << "count updated as " << count << endl;
		s.pop();
		count += getOnesCount(s,i-1);
	}
	else
	{
		count += (i-1)*(pow(10,i-2));
		cout << "count updated as " << count << endl;
		s.pop();
		count += stackToInt(s) + 1;
		cout << "count updated as " << count << endl;
		count += getOnesCount(s,i-1);
		cout << "count updated as " << count << endl;

	}
	return count;
}

int main()
{
	int n;
	cin >> n;
	string num;
	int count = 0;
	int temp = n;
	stack<int> s;
	while(temp)
	{
		int rem = temp%10;
		s.push(rem);
		temp = temp/10;
		count++;
	}
	cout << count << endl;;
	printStack(s);
	cout << endl;
	int res = getOnesCount(s, count);
	cout << res << endl;
	return 0;
}
